# Week 10

Cycle 2 test/deliver.

## Lesson: client presentations, reflection and celebration

Number of classes: 4

In last week's lessons students published their website and
prepared talking points for a 10 minute meeting with the client
(a class presentation). They will give that presentation this week as
part of their team's assessment.

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| People:<br>-roles and responsibilities | -the role of the web developer, project coordinator, content designers |
| Design, produce and evaluate:<br>-communication techniques<br>-methods of evaluation | -presenting the website to the client<br>-receiving peer and teacher feedback |

### Objectives

By the end of this week students will know:
- how to reflect on their and their team's performance in developing their websites
- the importance of celebrating achievements

Students can:
- communicate and justify reasons for design choices to clients
- identify what they've learned
- identify areas for improvement

### Teaching strategies

Classes 1 - 3 are used for teams to present their work.

There will also be time at the end of class 3 for teams to weight
their individual contributions to the project work, and provide
that to the teacher. The [assessment page](assessment.md#student-marks-for-teamwork) 
describes how teams are to determine the relative contributions of their team members.

They use the following spreadsheet to determine the relative contributions:

```{eval-rst}
:download:`teamwork mark spreadsheet <files/teamwork-mark.xlsx>`
```

In class 4:
- teacher gives the students their marks for interviews and presentations
- students do their final personal reflection
- we celebrate their achievements together
- students explore other team's websites which are on display

```{eval-rst}
:download:`personal-reflections-4 <files/personal-reflections-4.docx>`
```

### Assessment

This week teams present their websites, determine each member's
relative contributions to the project, and do their final
personal reflection.

See [assessment information for website presentation](assessment.md#website-presentation).

```{eval-rst}
:download:`teamwork mark spreadsheet <files/teamwork-mark.xlsx>`
```

```{eval-rst}
:download:`personal-reflections-4 <files/personal-reflections-4.docx>`
```
