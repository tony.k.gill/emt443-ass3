# Section B

This page addresses the requirements of Section B of the assessment.

## Three postings demonstrating background reading

### Posting 1 from week 4

Hi folks,

Here are my responses to the week 4 activities (now that I've got the first assessment out of the way!)

1. How does the IST syllabus lend itself to PBL?
 
- All core content, and points from the options, must be taught within the context of projects.
- So it is a project-based learning course.
- Students undertake projects in real-life contexts
- The entire project life-cycle is practiced, from conception to delivery of technology-based products
- Learning through investigation and collaboration with peers is emphasised
- Students are encouraged to reflect on their own learning to progress through projects
- Embraces students prior knowledge and skills and provides flexibility for them to explore their interests through choice of projects
- Looks to develop problem solving and critical thinking skills and skills in collaboration and team work
 

2. Post to the forum your own summary of the assessment requirements of the IST course.

It’s a framework, it uses standards as the reference point for planning teaching and learning, and assessing. Assessment is characterised by assessment for learning and assessment of learning (reporting). It’s driven by:

What is to be learned: defined by the outcomes and content in the syllabus.
And to what level: descriptions of levels of achievement in the syllabus
 
Standards are defined that guide teachers for planning programs. And help teachers and students set targets and monitor progress on their way to meeting those standards.

Examples are provided in the syllabus supporting documents.

Enables a learning plan to be created for students. And through self and peer assessment and teacher feedback students can assess their progress against the plan, and understand their next steps in the teaching process.

Assessments can occur in everyday lessons or be planned assessments.

There are a number of assessment for learning principles that guide teachers in reflecting on the purpose of the assessments:

- To track progress on student learning and plan future activities
- Students know where they are going, how they’re tracking, and what they’re doing next
- To foster a partnership between students and teachers with the goal of students gaining a deep understanding of the subject matter
- Feedback is individualised, constructive and assessed against the standards, not peers
- Geared to students taking responsibility for their own learning

Reporting. This is assessment of learning. The syllabus provides descriptions of levels of achievement to help with reporting. From level 1 to level 6. The level is reported on the school certificate of achievement at the end of stage 5.

3. Examine one of the IST option topics and describe a project (either by developing it yourself or by finding an example of the net) that will cover most of the particular option topic and relevant section of the core.

Project idea. Option 4. Digital media. 

Goal: Create a new character for cinematic universe, computer game, or television program.

Student instructions: The producers of your favourite television series (or cinematic universe or computer game) have called out to fans for new character ideas. They’ll take the most popular idea and write that character into their next release. You plan to strengthen your pitch by generating some buzz among the fans using engaging media. Your task is to create at least three digital media products to introduce your character. The fans and producers want to know your character’s backstory, their persona, their complex relationships with the other characters, and how the series will evolve with them in it. 

Description: Students develop skills in design and production by producing a number of digital media products. These include, but should not be limited to: character sketches, 3D models, animations, interviews with the character or character’s creators, theme music, costume design, and logos. In choosing their products students will examine the range of media products and their uses. Working in groups, students engage in an iterative design cycle, taking early ideas to fans (the classroom and their friends in the school yard and family), refining their ideas and seeking further feedback by making prototypes, and using those reflections to create their final products. They’ll seek feedback on those products and reflect what they’ve learned in the project and what they can improve on for the next project.

Duration: 15 wks

Outcomes: 5.1.1, 5.2.1, 5.2.2, 5.2.3, 5.3.1, 5.5.1, 5.5.2, 5.5.3

Students learn about:

- Project development
- Types of digital media products
- Data types for digital media products
- Manipulation techniques
- Digitisation
- Display and distribution
- Project development
- Evaluation of digital media products
- Designing possible solutions (C1)
- Producing solutions (C1)
- Methods of evaluation (C1)
- Management (C1)
- Communication techniques (C1)
- Collaboration and group work (C1)
- Data sources (C3)
- Data types and audio, video and animation (C3)
- Data storage and function (C3)
- Legal and social issues (C5)
- Roles and responsibilities and careers in IST (C6)
- Software systems, and their types (C7)
- Factors affecting hardware requirements (C7)

### Posting 2 from week 6

1. List some different assessment strategies

Formal summative assessment of learning include tests, essays, a product as part of a project.

Formal formative assessments for learning include be student-teacher interviews, weekly quizzes that are self-assessed.

Informal formative assessments for learning include strategic questioning, drawing a concept map, entry/exit tickets, developing prototypes to get early feedback, think-pair-share activities.

Informal formative assessments as learning include student self-reflection on their progress against the learning goals and success criteria on a regular basis.

2. Post to the forum an example of a lesson or assessment task you have either given or observed that you consider to be either rich or authentic.

In work I created an introductory programming course for staff in our team. The goal was demonstrated at the start of the course using a working example of a program that read and manipulated data to write a result. It was a small, but authentic, as it matched what was commonly done in that work environment – writing small programs to automate the reformatting of large amounts of data. At the beginning students were presented with a problem and wrote in words a procedure that they thought would solve the problem. Through discussion it was broken down into steps and the programming tools and constructs that could be used to implement each step were identified. Students were shown examples of those constructs, practised them, and used them to build up their program slowly over the two-day course. Students were asked to demonstrate to the class what their program was doing during the course; this promoted class discussion and students were learning from each other. It also allowed me to assess their progress and guide them to learn the next concept. Most students were able to self-guide by the end of the second day, as they began to recognise the concepts they still had to learn in order to complete the program.

3. Post to the forum how you could integrate either an element of literacy or numeracy into a PBL project. 

a) The option topic

The Internet and Web Development.

b) the project description.

In the previous project you created a new character for your favourite cinematic universe that you will pitch to the show’s creators. You now need to create an online persona for them. They need a home base. Somewhere that fans and the show’s creators can discover them. Something to generate buzz. Your task is to develop an engaging website to tell your character’s story and share this with your friends and family. Your website will be displayed at the end of year showcase.

c) Explain briefly how you could integrate numeracy or literacy in a non-trivial way to this topic:

Prior to creating the website, students are asked to create a poster of their website design to be presented to another group for feedback. The poster must show:
- Every page relative to the other pages
- The content to be placed on each page
- A brief written description of the purpose of the page – what message is it conveying about the character; how does it promote the character
- An annotated layout of the sections on each page, showing menu items, page body, headers, and footers
- Each student in the group takes a turn at presenting on an aspect of the website design to another group who assesses it against the success criteria for the design component, providing feedback on areas for improvement
- Literacy areas covered in the national literacy learning progression:
  - Speaking and listening: Listening, speaking and interacting
  - Reading and viewing: fluency and understanding texts
  - Writing: creating texts
- Numeracy areas covered:
  - Measurement and geometry: positioning and locating
  - National literacy and numeracy learning progressions:
  - https://educationstandards.nsw.edu.au/wps/portal/nesa/k-10/understanding-the-curriculum/literacy-and-numeracy/national-literacy-and-numeracy-progressions

### Posting 3 from week 11

Post if there was something new that excited you on the 60 in 60 site, or any ideas that you could seamlessly use to help enthuse your IST students. 
I've been thinking about how students can remain in contact outside the classroom and have seamless access to their shared documents - and do it using software that the Dept of Ed is invested in.

Google classroom is becoming entrenched in schools, just based on experience with my children's tools. It also seems that schools in NSW Dept of Education are adpoting Google workspaces, which is useful for collaboration on shared documents. However I don't think it allows separate channels for team discussions, which I expect some students will want to work on projects cooperatively outside the classroom. Slack is a good platform for creating such channels. However there's little document management.

One of the trends in the  last decade has been for online applications to focus on doing one thing well (e.g. slack is a good platform for team communication) and to leverage third-party apps using integrations, which are like plugins that communicate with third-party apps. It seems that Google and Slack are teaming up to create integrations so we can access Google documents from within Slack. Awesome.

https://workspace.google.com/slack/

Describe the issues that pertain to students working cooperatively and collaboratively and list some advantages and disadvantages of group work.
Cooperative learning is designed to achieve these goals:

Positive interdependence: the perception that an individual goal can only be achieved through team goals
Individual accountability: individuals are assessed with results given back to the group – does provide a chance for reflection and asking for and offering help
Promotive interaction: encouragement, advice, support are given
Social skills are taught just as academic skills would be: conflict resolution, communication, trust building, prosocial behaviour
Group processing is learned: that is, the group processes that the students are using to learn. Ask them to reflect and identify improvements and offer insights. The focus is on a process of continual improvement
 So cooperative learning works best when:

There are rewards for the group and there is individual accountability
The group reward is based on the sum of the individual contributions
Social cohesion within the group is high, with many trusting relationships
Group rewards are given based on group behaviours – i.e. positive interactions towards each other
So advantages are:

a social environment where students are supportive of each other
motivates through group goals
removes the feeling of competition which can be a demotivating factors
Disadvantages

Each cooperative learning activity can take time to design
and they can take time to enact in the classroom
not all students may engage meaningfully with the lesson, so close observation of the group work is required
Briefly summarise a techniques for cooperative learning from the online activity (or elsewhere), that someone else hasn’t done yet.
Jigsaw. Can be useful when there is a lot of content to cover and/or the teacher is not across all of it. The class is split into base teams (these might be the existing teams for the project). Each member of the team is assigned to a specialist group. The specialist groups form and are given a activity to complete, with a clear learning goal. The goal might be to research a question and identify several issues related to that question. Each student will contribute ideas and issues to the group to achieve the learning goal. The base teams then reform and each specialist teaches the others what they've learned. That creates a complete picture for all students of the topic content. The lesson may finish with a class discussion to summarise and clarify.

NESA provides the following list of resources to aid teaching coding (http://educationstandards.nsw.edu.au/wps/portal/nesa/k-10/learning-areas/technologies/coding-across-the-curriculum).  Choose AT LEAST TWO that someone hasn't yet commented on in the weekly forum, and describe how they could be integrated into a PBL project or used to direct a student who wants to be further stimulated.
Not on the website, but I've been looking at the features of webflow for assignment 3. It allows website developers to visually create content. If they choose to they can edit the HTML or CSS or javascript directly. So this could be an effective way to ease students into modifying website content using the underlying 'language'. They can make modifications in one mode (visual or text based) and instantly see how it changes in the other. Thus they can develop skills in moving fluently between those representations.

If a student is getting deeper into text-based coding, then I think a good language to teach the fundamentals is Python. However, when starting, a barrier can be installing the software (especially at school) and getting it to run (setting paths, understanding versions). Trinket provides a nice online experience where students can just focus on the coding: https://trinket.io/features/python3. And running it!

## Two postings contributing to a discussion thread

### Posting 1 from week 1

**Greg A:** I'd have to say that our society generally is moving towards removing creativity in solutions, using checklists to ensure minimum standards are met, but in doing so also removing the risky creative, stunning solutions. And I would assume these expectations are part of what is happening for teachers too. 

Risk taking as a teacher implies a possibility of failure, and that is likely to be unacceptable to the Department! 

But just as Davies discussed, removing risk taking reduces the quality of learning of students, restricts student ability to step out of comfort zones, and drops student motivation to perform except with regards to getting a good test result. No doubt the same issues occur in the motivation of teachers. Risk is needed for the teachers and for the students.

And, paradoxically, there's enough data to say that the future of our kids is more risky if we don't take risks. 

Being creative and taking a risk is supported by popular belief too. Our entertainment shows teachers trying out something non-traditional to inspire kids (and overcoming the traditionalists trying to stop them), and ending by making a huge difference in their student's lives. Heroes. So there is some societal support of risk taking, perhaps only if it is successful!

Overall, I think we'll have no choice. We are being taught non-traditional teaching methods that will be more successful. But we may also be shoehorned into not being more innovative than the prescribed level of innovation. Creating a new traditional teaching of previously non-traditional methods, with a new type of risk taker pushing education and students forward.

**Andre K:** Greg you make an excellent point about non-traditional becomming the new traditional.  I guess I have found that I certainly feel that I change my style according to the class.

Personally, I like flexibility in my class structure with a lot of dry and sarcastic humour, however one year I had 5 kids high on the spectrum.  This type of humour and lots of flexibility in class structure drove them mad.  SO I had to quickly offer them something totally different to keep them sane and productive.  As you say, one size doesn't fit all, and our own teaching style encompassess a lot of our own preferences.  We need to be aware of where all of our students are at and try to "individualise" as much as possible as well.

So certain personality types / and some students on Individualised Learning Plans absolutely loathe PBL and co-operative learning.  The trick is guiding them through this, so that they are better able to cope with University and the reality that industries (including IT) demand a lot of co-operative work and being able to manage these teams.

**Tony G:** Greg, I was a bit surprised by your response that society is generally moving towards removing creativity. That hasn't been my experience at work - admittedly in academia, research and most recently in the public service in a team with a culture of problem solving and continual improvement.

Throughout my working life I've also been encouraged to innovate. And I think we'll need to be innovative in our classrooms. But what is innovation? For me it is working within constraints (time, cost, people/attitudes) to create something new or refine something that already exists to achieve an outcome.

So what might innovation mean in the teaching context? I think the outcome is to improve student achievement and grow their capabilities. So we'll identify what the students need, the innovative part is to introduce new teaching practices to the school, or to refine the existing ones. The challenge will be working within the constraints: your knowledge, the school's culture/attitudes, available resources, time pressure, quality of delivery. The proof will be whether the new methods improve student outcomes without costing extra time or resources while at the same time changing attitudes and growing your knowledge and skills (and other benefits as yet to be imagined).

**Andre K** A organisational consultant once explained to me that in broad terms traditionally there is a US and British approach.  The Americans are results orientated and the British are more procedural.

So certainly the rhetoric in many organisations is "innovation", but the question is how is "failure" assessed.  Is it: "we value innovation and value that you tried something new, what have you learned?"  OR "We value innovation, but your failure was probably not following policy and procedures, please ensure you comply within those boundaries."

No school will ever say that they don't value innovation and creativity in their teachers, but many schools will want you to do what everyone else is already doing.  Anecdotally, a friend was head of department in a high fee paying school with excellent results.  He wanted to try to change how Mathematics was taught and explained that the research was behind him.  However, the risk of maybe failing or teething problems, meant that he was advised to keep doing more of the same.

Choose where you work carefully if you want to be different.  A great revealing question in the interview is to ask: Can you tell me about an excellent teacher in your staff and why they are excellent, and can you tell me about a teacher who didn't work out in your school and why.


### Posting 2 from week 3

**Tony G:** For assessment 1, we're asked to assess the effectiveness of project-based learning for developing students' ICT capabilities. The primary purpose of the NSW IST curriculum is to develop ICT capabilities . The syllabus is clear that this is to be achieved with project-based learning (NSW B.o.S., 2003). There is no other option. This will be a challenge for me. For example, what are the best approaches to teaching the minutiea and complexity of programming concepts to students who are focussed on a larger project?

Looking through the syllabus I can see why this approach was taken. It is now almost 20 years old, and most of the core content to be taught is still relevant. Decoupling the content and outcomes from the technology used to deliver it has ensured its longevity. With regards to the topics, I'm a little perplexed why AI was grouped with simulation and modelling, and why robotics was grouped with automated systems. You can still have those things without needing the other. If we choose to teach the AI, simulation and modelling option, do we have to teach all of it or could we drop the AI component as long as we cover the core content with other topics?

**Andre K:** At the time of publication and for a Year 9/10 student target group, most AI projects included setting up programs such as 20Q, or some other classificatory program.  Of course this is a long way from where AI has come, which is a bit more reflected in the rewrite. 

Keep thinking, how could you teach the big ideas and how to approach the problem to students without sophisticated programming skills or who may be doing your subject because they thought they would just be playing computer games all the time.

**Tony G:** Thanks Andre,

I posted in the week 4 forum about the AnyLogic software that I came across. It looks very promising. Prior to this I was thinking how students might be able to use excel to create simple models. But I felt very uneasy with that idea - no matter how engaging the story, excel is still pretty unexciting. Don't get me wrong, it is still the right tool in some cases, and I would have no problems in students identifying it as the right tool and using it. However, I think AnyLogic will be far more engaging and it contains algorithms for the types of AI methods referred to in the syllabus, plus more! My concern would be about complexity and the size of the learning curve. Until I download it and attempt tutorials I'll remain cautiously optimistic.

And for the gamers, this software allows you to tweak parameters and watch how your simulations change. The animations and visualisations look very engaging. The concepts learned here can be brought into a gaming environment later.

[Anylogic, for AI simulation and modelling](https://www.anylogic.com).

**Andre K:** Thanks for the detailed description.  Keep us posted if you play with it.

_(Note: I have not had an opportunity to assess the AnyLogic software)_

## Table of resources

A collation of the resources I posted to the discussion forum throughout the semester.

These are in addition to those listed on [the resource page](resources.md) for this assessment.

|Resource|purpose|upload date|
|--------|-------|-----------|
|[Anylogic](https://www.anylogic.com)| AI, simulation and modelling | 26/3/2022 |
|[EarthBlox](https://www.earthblox.io/) | Visual programming of satellite images for modelling | 26/3/2022 |
|[Gamemaker studio 2](https://www.yoyogames.com/en/get) | Computer programming of games | 26/3/2022 |
|[wix](https://www.wix.com/) | web development tool | 20/4/2022 |
|[w3schools](https://w3schools.com) | self-directed tutorials on coding and markup languages | 27/4/2022 |
|[webflow university](https://university.webflow.com/)|tutorials on website design, with a focus on the tools at webflow.com| 27/4/2022 |
|[quackit database tutorial](https://www.quackit.com/database/tutorial/)|pitched at those with no database knowledge | 27/4/2022|
|[slack](https://workspace.google.com/slack/)| For online team communication - soon to integrate with Google workspace, which NSW public schools use. | 5/5/2022 |
| [Trinket](https://trinket.io/features/python3) | For learning python online, without downloading and installing | 5/5/2022 |
