# Week 7
 
Cycle 2 build.

## Lesson: website build sprint 1

Number of classes: 4

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| People:<br>-roles and responsibilities | -the role of the web developer, project coordinator, content designers |
| Design, produce and evaluate:<br>-producing solutions<br>-management<br>-communication techniques | -creating a website<br>-tracking project progress through meetings, record keeping and kanban boards |

### Objectives

By the end of this week students will know:
- processes for monitoring project progress
- tools for creating and publishing websites

Students can:
- use software to create websites and website content
- publish a website
- track project progress
- identify issues and suggest solutions


### Teaching strategies

Project work.

Remind students to:
- engage in scrums to identify and solve issues
- scrum master to maintain documentation
- reflect on and document task progress
- keep the kanban up to date
- work out how to use webflow to publish your site online by the end of the week (this should be a task)
- refer to the [rubric](assessment.md#rubric) and use the assessment requirements to help with decision making
- circulate between the groups through the week, providing feedback and guidance, just-in-time teaching, and keeping students on track

### Assessment

Teacher makes observations about students' competencies using the software packages.
See [assessment for website and/or project coordinator elements](assessment.md#website-andor-project-coordinator-elements).
