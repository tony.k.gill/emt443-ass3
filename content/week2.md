# Week 2

Cycle 1. Ideate.

By the end of this week students will gain a greater appreciation for the Internet
and the World Wide Web, understand how websites complement other Internet platforms,
consider the type of website that best solves their problem, understand elements of
good website design, and identify the content requirements of their site.

## Lesson: World Wide Web considerations

Number of classes: 2

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| Past, current and emerging technologies  | -web 1.0, web 2.0, and web 3.0<br>social media, music and video streaming services<br>-types of websites (business, personal profile, blog, cv, portal)       |

### Objectives

By the end of this lesson students will know:
- the development of the key Internet technologies and WWW technologies that have changed website design 
- the differences between website types and their purposes
- how to learn in specialist teams

Students can:
- explain how websites complement other Internet services
- understand and communicate a range of factors that affect website design
- communicate website types and their purpose
- work together to choose a website type and justify their choice

### Teaching strategies

There's a lot of content knowledge to cover this lesson. So we'll share the load by
doing a cooperative **Jigsaw**, splitting
teams into specialist groups to research a particular aspect of web design.
Students will reconvene into their teams afterwards and share knowledge by answering questions
on a worksheet together, and then having a whole-class discussions about those
questions. Each team will identify an appropriate website type to solve their problem.

**Engage: a (very) brief history of website design**

10 minutes

Watch this video on
[the history of website design](https://www.smamarketing.net/blog/the-history-of-website-design).

Outline the next two classes for students:
1. Five specialist groups will be formed (assuming there are five teams with 5 people each) created from one person from each team
2. Each specialist group is given a different question to research with the goal of creating a brief, dot-point, report about the topic
3. Each person in the group will conduct their own research on that question and identify three ideas or concepts
4. They will share their ideas with the specialist group, with each person recording others' ideas
   on a handout
5. Teams reconvene and use their research to answer a series of questions about the world wide web and what they
   must consider when designing websites
6. Discuss those responses as a class, to ensure they've identified the key points
7. Teams decide on the type of website to solve their problem

Create the teams. Provide a handout to the specialist groups which contains the focus questions.

```{eval-rst}
:download:`WWW considerations specialist worksheet <files/www-considerations-specialist-q.docx>`
```

**Explore. Individual research**

15 minutes

At the same tables as their specialist groups, all students start on the 
[the history of website design](https://www.smamarketing.net/blog/the-history-of-website-design)
page. They can use the links within or do their own searches to research their question.
Each student will identify three ideas or concepts that answers their question.

**Explore. Specialist group discussion**

20 minutes

In their groups each student is given a maximum of 3 minutes to share their ideas.
Others may ask clarifying questions. All students note the ideas on the handout.

End of class 1.

**Explore. Team activity - worksheet**

Start of class 2.

20 minutes

Back in their original teams, students exchange knowledge about their specialist
area. The worksheet contains focus questions to guide the discussion.
Each student will make notes on their worksheet.

```{eval-rst}
:download:`WWW considerations team worksheet <files/www-considerations-team-q.docx>`
```

**Explore. Team activity - website type**

15 minutes

In your teams, learn about different types of websites from your specialist, and discuss
what you think the best type of website will be to solve your problem. 

In the decision log:
- capture the key points of the discussion and who made them
- document the team's decision in the decision log
- provide three points to justify this choice in the decision log, thinking
  about your user's needs, your client's needs, and world-wide-web considerations

```{eval-rst}
:download:`meeting record <files/team-meeting-decision-log.docx>`
```

Teacher moves between tables and discusses their choice and justification, and provides
formative feedback to guide them to an appropriate website type.

**Explore. Class discussion**

10 minutes

With reference to the answers to the worksheet questions, invite each team to share
one issue they must consider when designing their website.

End of class.

### Resources

- [the history of website design](https://www.smamarketing.net/blog/the-history-of-website-design)

```{eval-rst}
:download:`WWW considerations specialist worksheet <files/www-considerations-specialist-q.docx>`
```

```{eval-rst}
:download:`WWW considerations team worksheet <files/www-considerations-team-q.docx>`
```

```{eval-rst}
:download:`meeting record <files/team-meeting-decision-log.docx>`
```

## Lesson: Content and design considerations

Number of classes: 2

Class number 2 will be used as the first teamwork observation assessment.

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| Design, produce and evaluate:<br>-designing possible solutions<br>-collaboration and group work | -understand what makes engaging websites<br>-website structure<br>-page layouts<br>-navigation and links<br>-content elements<br>-URLs<br>-identify and document site features<br>-investigate websites and examine features |
| People:<br>-roles and responsibilities | -Identify the roles in web design teams |

### Objectives

By the end of this lesson students will know:
- how websites are structured
- the content elements of a website
- how web pages are layed out
- the roles and responsibilities of members of a web design team

Students can:
- discuss and negotiate the elements of their website
- document decisions on the eight website elements that they will use on their website and their justification
- identify how they will (or hypothetically would) integrate the digital media products created in project 1


### Teaching strategies

Mix cooperative learning with scaffolded instruction

**Class 1**

- Engage students in the class by examining [the year of Greta](https://theyearofgreta.com/), which uses a website to create what could be described as a museum exhibition
- Class discussion: what is engaging about this site?
- Describe the purpose of the lesson, which is to identify key content and design considerations for creating their website, and the roles and responsibilities of people who create them
- In their teams:
  - two people will investigate the question of the importance of an engaging website and what makes a website engaging: watch [five great website designs](https://www.youtube.com/watch?v=AmHEfTSBXiY) or read [the importance of great visual design](https://cxl.com/blog/first-impressions-matter-the-importance-of-great-visual-design/) 
  - one person will investigate what _content_ could be used to make the website engaging (read or watch the resources in the previous point)
  - one person to study the website [karliekcloss.com](https://www.karliekloss.com/):
    - take screenshot of the main page and annotate it with the elements
    - create a simple [site map](https://slickplan.com/sitemap/what-is-a-site-map), which shows the structure of the website (a sketch of the sitemap on paper is fine)
  - one person to consider the [roles and responsibilities](https://www.websiteplanet.com/blog/web-designer-vs-web-developer/) involved in building a website
- Finish the class by stating that the first of three teamwork and collaboration assessment observations will be done in the next class; one criteria is arriving promptly to class; other criteria will be introduced in the class

**Class 2**

Engage the students by asking them to watch the following two-minute video about teamwork and problem solving.

[The Big Bang Theory on teamwork](https://www.youtube.com/watch?v=8Amu3UBj-qw)

Lead a class discussion using these questions from the rubric:

- all team members constructively contribute ideas to the discussion and invite ideas from others
- all team members respectfully negotiate disagreements, providing justification for their ideas

Ask them to give the Big Bang Theory team a mark out of 4 for each criteria and explain why.

Highlight the other requirements in the [rubric](assessment.md#rubric) for record keeping and documentation.

- In this class the students, in their teams, will share and use the knowledge gained from the previous day's research to:
  - agree on _at least seven_ new content elements that must be included in their website, and justify their choices in terms of website engagement, good design or being related to the problem statement
  - identify the roles within a web development that are responsbible for creating those elements
  - document the key points of the discussion and the decisions made in the decision log 

```{eval-rst}
:download:`teamwork observation assessment sheet <files/teamwork-observation-sessions.docx>`
```

### Resources

- [five great website designs](https://www.youtube.com/watch?v=AmHEfTSBXiY)
- [the importance of great visual design](https://cxl.com/blog/first-impressions-matter-the-importance-of-great-visual-design/)
- [what is a site map](https://slickplan.com/sitemap/what-is-a-site-map) 
- [roles and responsibilities in website development](https://www.websiteplanet.com/blog/web-designer-vs-web-developer/)
- [karliekcloss.com](https://www.karliekloss.com/)
- [The Big Bang Theory on teamwork](https://www.youtube.com/watch?v=8Amu3UBj-qw)

```{eval-rst}
:download:`meeting record <files/team-meeting-decision-log.docx>`
```

### Assessment

The first observation meeting is held this week. See [teamwork assessment](assessment.md#teamwork).

```{eval-rst}
:download:`teamwork observation assessment sheet <files/teamwork-observation-sessions.docx>`
```
