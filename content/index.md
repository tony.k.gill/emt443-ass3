# The Internet and web development

EMT443 assessment 3

Tony Gill 94069094

Due date: 23 May 2022

## Background

This ten week project was developed by Tony Gill as part of his
pre-service teacher training at Charles Sturt University, for 
the subject EMT443.

The project was designed on the assumption that there would be four 45-50
minute classes each week.

It introduces students to website design, development, and publishing 
for the NSW IST stage 5 syllabus option The Internet and Web Development.

As part of the assessment I was required to identify two lessons that
demonstrate cooperative learning. And two that demonstrate explicit
scaffolded instruction.

The two lessons that describe cooperative learning are:
- [world wide web considerations](week2.md#lesson-world-wide-web-considerations)
- [gallery walk](week3.md#lesson-gallery-walk)
  - this lesson is [continued in week 4](week4.md#lesson-gallery-walk-continued)

The two lessons that decribe explicit scaffolded instruction are:
- [define the problem](week1.md#lesson-define-the-problem)
- [identifying tasks and planning](week6.md#lesson-identifying-tasks-and-commence-website-build)

Note that each lesson occurs across multiple classes.

Another part of the assessment - [Section B](section-b.md) - required the collation of evidence
of forum participation throughout the semester.

## How to navigate this site

There are multiple options:
- the [project overview page](contents.md) provides a complete picture of the entire course on one page
- use the menu items to the left to jump to a week or a lesson within that week
- use the _next_ and _previous_ buttons at the bottom of each page
- the [assessment page](assessment.md) describes each assessment with a link to the rubric
- the [resources page](resources.md) provides a list of all resources used in the project
  - the resources used for each lesson are listed with that lesson
- the [section B page](section-b.md) lists my contributions to the forum

## Context within the scope and sequence

In project 1, students worked in teams to design a character for their favourite TV show,
cinematic universe, or game, under the premise that the show's creators were on the 
look-out for a new character to be included in the next release. They created three
digital media products that describes their character's background and identity that
can be used as part of a pitch to the show's creators. They now turn their attention
towards showcasing the character, in the form of a website.

```{eval-rst}
:download:`scope and sequence plan <files/scope-and-sequence.docx>`
```

## Teacher brief

Students work in teams (same teams from Project 1) that extends on the work done in Project 1.
They are a team of website developers and a client has asked them to create a website that
will engage the creators of a TV show (or computer game or cinematic universe)
to convince them that this character must be written
into the next release. They will empathise with the show's creators, define the problem to be solved,
learn about Internet and web technologies to gather ideas for their website, prototype and
build the solution, and test it using student and teacher feedback.

Students become familiar with technologies for creating web sites, and some of the history of
Internet development. By evaluating the design and functionality of a number of websites, they learn
important design and technical elements. They become familiar with static and
dynamic websites and web-based applications, the tools used to create and publish them,
and the roles and responsibilities of web-development teams. In teams they plan, design,
and publish their own website. They’ll create prototypes, seek class feedback, and refine and publish the site.


## Student brief

You are a team of website developers. You've been approached by a character developer to create a web
presence for a character they want to pitch to the show's creators.
They have provided you with some digital media products that they've already created, however more
content is required for the website.

Your website will form part of a virtual community you're building around the character.
It will be targeted at the show's creator as part of your bid to have them
write the character into the next release.

It's a challenging task that requires every bit of your team's creativity,
problem-solving ability, technical know-how, and commitment to make it happen.
It can't be achieved on your own.

## Project outline

### Learning goals

Outcomes taught:

- 5.2.1 describes and applies problem-solving processes when creating solutions
- 5.3.1 justifies responsible practices and ethical use of information and software technology
- 5.4.1 analyses the effects of past, current and emerging information and software technologies on the individual and society

Outcomes assessed:

- 5.1.1 selects and justifies the application of appropriate software programs to a range of tasks
- 5.2.2 designs, produces and evaluates appropriate solutions to a range of challenging problems
- 5.2.3 critically analyses decision making processes in a range of information and software solutions
- 5.5.1 applies collaborative work practices to complete tasks
- 5.5.2 communicates ideas, processes and solutions to a targeted audience
- 5.5.3 describes and compares key roles and responsibilities of people in the field of information and software technology

Students learn about: The Internet and historical perspectives; uses of the Internet; Internet software; Internet transmission/transport protocols; www; information access controls; website development and publishing; features of a website; search engine optimisation; project development; defining and analysing the problem (C1); designing possible solutions (C1); producing solutions (C1); evaluation criteria (C1); collaboration and group work (C1); The impact of past, current and emerging information and software technologies (C2); troubleshooting (C4); legal issues (C5); roles and responsibilities (C6); types and examples of software (C7); applications including bespoke (C7); features and elements of a GUI (C7).

Students learn to: clearly articulate the design problem; test solutions using prototypes;
describe virtual communities and the Internet technologies used to create them;
use online web-site builder software to create and publish a website; create website content using
graphic design software; matching the design elements to the cultural identity of their character;
track progress using project management tools; discuss and negotiate solutions to issues;
negotiate and assign roles and responsibilities to achieve team goals; reflect on feedback to
improve solutions.


### Artefacts

Team artefacts:

- prototypes
- a published website
- skills matrix (not assessed)
- classroom resources (not assessed)
- kanban board (not assessed)

Personal artefacts:

- digital products for website
- website element designs in a portfolio
- project tracking documentation

See [the assessment page](assessment.md).

### Project duration, cyles, phases, lessons and classes

The project is expected to take 10 weeks to complete. It is enacted across
two design-thinking cycles. In cycle 1, students
create and assess prototype websites. In cycle 2, students create the
website content, the website and publish it.

Each cycle is composed of phases, based on stages in the design thinking process, of:

1. empathise
2. define the problem
3. ideate
4. prototype / build
5. test / deliver

Each cycle is broken into multiple lessons. Lessons occur over one or more classes.

Several weeks are dedicated in cycle 2 to students collaboratively building
the website. Teacher's discretion must be exercised when deciding if the
project duration should be extended or shortened depending on student progress. 

