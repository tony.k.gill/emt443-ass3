## Lesson: INSERT NAME 

Number of classes: N

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
|      |        |

### Objectives

By the end of this lesson students will know:
- learning goal 1
- learning goal 2
- etc

Students can:
- skill 1
- skill 2
- etc


### Teaching strategies

**Authentic instruction**

Regardless of the pedagogy, all lessons must have five standards of authentic instruction:

1. promote higher order thinking
2. depth of knowledge: do fewer tasks, but in depth
3. connectedness to the world beyond the classroom: real-world activites, linked to other subjects, linked to lived experience
4. substantive conversation: one or two topics discussed with two to three exchanges per question across the class
5. social support for school achievement (e.g. high expectations, respect, responsiveness)

**Higher order thinking**

From low order thinking, to high order thinking, set a learning goal for each of:

1. remember
2. understand
3. apply
4. analyse
5. evaluate
6. create

- Use the pedagogy wheel as a guide: https://designingoutcomes.com/english-speaking-world-v5-0/
- Use one of the action verbs in each category when writing a learning goal
- What activities will students do?
- Which software will be used?

**Appropriate pedagogy**

Will depend on content, resources, students

One of three:

1. Cooperative learning
2. Explicit scaffolded instruction
3. Project work

Cooperative/Collaborative learning models have these attributes:

- positive interdependence
- individual accountability
- face-to-face promotive interaction
- appropriate use of social, interpersonal, collaborative and small-group skills
- group processing

For Explicit scaffolded instruction (similar to St John's ACT model):
- Engage
- Explore
- Summarise and connect

### Resources

These include content materials, templates or scaffolds for students, and technologies.

Consider the framework for choosing appropriate technologies:

- Provide a rank in each of four pillars:
  - Cognitively Active
  - Engaging, kids stay focussed
  - Meaningful
  - Social
- Then provide a rank for whether it helps support a learning goal
- Assess pedigree on a grid (two axes):
   - a combined score for the pillars on one axis (high or low)
   - its educational context (high or low) on another
   - we want both to be high, which equates to deep learning

### Assessment

How are the lesson objectives assessed?

Formative or summative.

What techniques will you use to provide student feedbacks with feedback on their learning?

