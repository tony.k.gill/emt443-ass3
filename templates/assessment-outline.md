The following template is for one assessment task. It is based on the assessment
descriptions given on the NESA website.
[For example:](https://educationstandards.nsw.edu.au/wps/portal/nesa/k-10/resources/sample-work/sample-work-detail/technologies/sample-work-info-software-tech-activities-st5-digital-media-project)

The criteria for assessing learning are given in the rubric.

Ideally I'd also have annotated examples.

### Design portfolios

Value: 25 marks

Due: last class of week 4

**Description of activity**

Students develop a portfolio of ideas that help them design their websites. The portfolio must
contain a minimum of one idea from each of the following stages in cycle 1 of the project:
- empathise
- define the problem
- ideate
- prototype

Each idea must be accompanied with a one paragraph explanation of how it meets the user's or client's needs.


**Context**

In cycle one of the project students examine the requirements of the project from the user and client
perspectives. This gives the website development task a clear purpose and direction. They must
demonstrate engagement in a genuinue process of understanding the problem from the user's perspective
and a clear justification for their ideas and their contribution to the prototype.

**Outcomes**

5.2.2: designs, produces and evaluates appropriate solutions to a range of challenging problems.
