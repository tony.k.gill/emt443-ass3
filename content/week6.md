# Week 6

Cycle 2 ideate to build.

This week we finish the software evaluations lesson.
Students also assign roles and responsibilites using
a kanban board, reflect on their personal goals for the next
three weeks, and commence the formal build stage.

It will also include a team observation and personal reflection
as part of the assessment.

## Lesson: software evaluation continued

This is a continuation of
[last week's lesson](week5.md#lesson-software-evaluation-role-selection-and-task-identification).
Refer back to last week for the syllabus concepts and skills covered, the objectives and class 1 instructions.

### Teaching strategies

Cooperative learning: jigsaw.

**Class 2**

_Re-engage in previous lesson. 10 minutes._

Ask students to continue the research into the software they will use to design and build websites.
Continue to collate information in the software features worksheet
from [class 1](week5.md#teaching-strategies-1).

_Create learning resources. 20 minutes._

In their specialist groups, students take turns to discuss what they learned through their
research, and how they think it will help them create their content or website.
They add (copy) their information to a shared software features worksheet. This 
becomes a classroom resource. 

```{eval-rst}
:download:`software features worksheet <files/software-features-worksheet.docx>`
```

_Share knowledge with teams. 15 minutes._

Students reconvene in their teams. They each describe one element they think
they can create for the team's website and the software package's tools and features
that will help them create it.

Remind students that the next class will include teamwork observations.

### Resources

```{eval-rst}
:download:`software features worksheet <files/software-features-worksheet.docx>`
```

## Lesson: identifying tasks and commence website build

Number of classes: 3

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| Design, produce and evaluate:<br>-planning<br>-management<br>-collaboration and group work<br>-producing solutions<br>-communication techniques<br>-methods of evaluation | <br>-identify and assign roles of a website development team<br>-identify and assign website-building tasks<br>-estimate task and project duration<br>-create a kanban board for communicating responsibilities and progress<br>-commence website build<br>-reflect on the assessment criteria to ensure their tasks will meet the requirements for assessment |
| People | Understand the roles of the project coordinator, web developers, and content designers. Know what skills are required and use this knowledge to assign roles to team members. |


### Objectives

By the end of this lesson students will know:
- their role and responsibilities within their team, one (or two) of:
  - content designers
  - web developers
  - project coordinator
- SMART criteria for identifying tasks:
  - specific: aligned to creating an output
  - measurable: how many of a particular thing do you want
  - achievable: aligned with skills of the team
  - relevant: directly related to solving the user's need
  - time-bound: can it be done in time allocated
- kanban boards for tracking project progress

Students can:
- negotiate their roles and responsibilities within their team
- create SMART tasks for their website development
  - use the assessment [rubric](assessment.md#rubric) to identify the website development elements they must create
    in their role to meet the assessment criteria 
- create a kanban board

### Teaching strategies

Use explicit scaffolded instruction so students achieve the lesson objectives.

**Preparation**

- in planning the teaching of this project, it was determined that students must
  be able to set realistic, relevant goals for creating the website, and to
  learn a method for monitoring the team's progress in achieving their goal
- it was decided that a kanban board was appropriate given:
  - relatively simple to create and use, given limited project time
  - it is visual and be put on public display to hold all students accountable for progress
- staff card and task card templates were created to help students document
  their roles and responsibilities
- it was ascertained that the knowledge gained from previous lessons about roles
  and responsibilities, constructive teamwork, and technical aspects of website
  development are prerequisites for selecting and negotiating their roles and tasks

**Start the lesson**

15 minutes.

Start this lesson by engaging in a reflective, think-pair-share
exercise about team achievement and roles.

To date, students have worked together to create a detailed prototype
for their website, which clearly addresses a user's need. They've also
done Jigsaw exercisees to create classroom resources.

While watching the following video, ask students to _think_ about what their teams
and the specialist groups have created thus far and what they've contributed to
personally. 

[Teamwork](https://www.youtube.com/watch?v=qh4ms4xgHko)

In _pairs_ discuss:
- what has your team and class achieved?
- how much of this could they have achieved on their own?
- what do they think of this cooperative approach to learning and achievement? 

Ask students to _share_ what they discussed with the class. Encourage them to refer to
the class resources they created and how they will be of benefit.

Explain the lesson goals as outlined above.

**Learn: Assigning roles and creating tasks**

30 minutes.

Clearly state to the class that the roles to be filled by the team are:
- content developers
- website developers
- a project coordinator (only 1)
- a student can take on up to 2 roles (the project coordinator _must_ take on a second role)

Discuss [the assessment information for website elements](assessment.md#website-andor-project-coordinator-elements)
and the [rubric](assessment.md#rubric) to ensure students are aware of the
requirements for the role(s) they have taken on.

Show the staff cards in the kanban planning resource and how each student is to have
one, each of a different colour.

```{eval-rst}
:download:`kanban planning <files/kanban-planning.pptx>`
```

Next, state that once they have chosen their roles they are to identify the tasks
they will complete to build the website. Explain a SMART task. Demonstrate writing a
SMART task on one of the task cards. A
SMART task will: be aligned with implementing an aspect of their prototype, identify
how many are needed, be aligned with the capabilities of the team, be part of 
solving the end user's needs, and have a realistic estimate of time within the
bounds of the project.

Make sure students are aware that as part of their assessment requirements,
it is the project coordinator's role to ensure an
accurate record of _this_ meeting is kept using the decision log.

```{eval-rst}
:download:`meeting record <files/team-meeting-decision-log.docx>`
```

Each team now works together to identify roles. Each student then writes
three tasks and gets feedback from other team members on how SMART their
task is, and how it can be refined to help them complete the project.

In negotiating their roles and tasks students will revisit and update (as needed) the
skills matrix developed in week 1.

```{eval-rst}
:download:`skills matrix <files/skills-matrix-template.xlsx>`
```

The teacher circulates between groups. Ask students to explain their tasks
and why they think they are SMART. Guide students to prioritise tasks that
will ensure a high-quality website is produced that meets the assessment requirements.

End of class 1.

**Learn: starting a kanban board**

Class 2.

Rengage. Students have identified their roles and responsibilities and commenced
creating tasks. The purpose of today's class is to coordinate their tasks to
successfully build the website.

Now introduce kanban and sprints (15 mins).

Watch [intro to kanban](https://www.youtube.com/watch?v=R8dYLbJiTUE) (watch to 2:45 mark).

Discuss key concepts:
- the task cards
- the columns
- the work-in-progress limit

Demonstrate adding a kanban board to the pin board and adding the staff cards
and some task cards under the backlog and ongoing columns.

Next, watch [intro to scrum](https://www.youtube.com/channel/UCqYarI0avGozOjvpN3y1l7A) from 3:30 to 7:00.

Discuss key concepts:
- the sprint contains tasks
- in our case, we'll have three sprints that each last 5, 4, and 4 classes
- at the end of each sprint you will publish a version of your website online
- at the end of the third sprint you will also have prepared talking points for
  your client meeting (class presentation), so it must include those preparation tasks
- we won't use the burndown chart as described in the video, as our project is much simpler
- you'll use scrums to keep your project on track, your project coordinator is your scrum master

Team activity, which is also used for the teamwork observation assessment (25 mins).

```{eval-rst}
:download:`teamwork observation assessment sheet <files/teamwork-observation-sessions.docx>`
```

In this activity you use the knowledge just gained with the assistance of 
the instructions on the kanban planning worksheet to:
- add the kanban board and set the work-in-progress limit
- allocate tasks to each of the three sprints
- add sprint 1 tasks to the board
- you will also decide the frequency at which you have scrum meetings? (is one meeting per class a good use of time? what will you do in each meeting?)

```{eval-rst}
:download:`kanban planning <files/kanban-planning.pptx>`
```

End of class (5 mins)

Explain that over the next three weeks, the team (led by the project coordinator) will keep the
kanban board up to date and use it to identify issues and bottlenecks.
Your tasks, and the progress you're making on them, are visible for the entire
class to see. You will get very good at using kanban to track your projects the
more you use it.

End of class 2.

**End of lesson: self-assessment of tasks**

Start of class 3.

The end of the lesson is a personal reflection (15 mins) on how students can self-assess
their website tasks.
See [assessment for personal reflections](assessment.md#personal-reflections).

```{eval-rst}
:download:`personal reflections 3 <files/personal-reflections-3.docx>`
```

The purpose is for students to review what they've learned in the preceding
lessons and align that with the requirements for their individual assessment,
and to appreciate how that fits with the broader team goal now expressed
in the form of a kanban board.

Students commence website building!

### Resources

- [musical teamwork](https://www.youtube.com/watch?v=qh4ms4xgHko)
- [intro to kanban](https://www.youtube.com/watch?v=R8dYLbJiTUE)
- [intro to scrum](https://www.youtube.com/channel/UCqYarI0avGozOjvpN3y1l7A)

```{eval-rst}
:download:`meeting record <files/team-meeting-decision-log.docx>`
```

```{eval-rst}
:download:`kanban planning <files/kanban-planning.pptx>`
```

```{eval-rst}
:download:`personal reflections 3 <files/personal-reflections-3.docx>`
```

```{eval-rst}
:download:`skills matrix <files/skills-matrix-template.xlsx>`
```

### Assessment

Team work observations 3 held in class 2. See [teamwork observations](assessment.md#teamwork).

```{eval-rst}
:download:`teamwork observation assessment sheet <files/teamwork-observation-sessions.docx>`
```

Personal reflection 3.
See [assessment for personal reflections](assessment.md#personal-reflections).

```{eval-rst}
:download:`personal reflections 3 <files/personal-reflections-3.docx>`
```
