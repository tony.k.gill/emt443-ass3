# Week 4

Commence project cycle 2.

By the end of this week the teams will have used the feedback provided in the
gallery walk to redefine the problem and improve the prototypes.

They will take some time to reflect personally on their knowledge, progress,
and contributions to the team, and discuss as a team how they can improve
group processes.

In their personal reflections students will identify the ideas they
would like to submit as part of their design portfolios, collate
evidence of those ideas into a portfolio and submit it for assessment.

We'll then commence our lessons on the technical considerations of
building websites.


## Lesson: gallery walk continued

Students use the feedback from the gallery walk to empathise,
redefine the problem, and modify their prototype.

See [week3 for the concepts and objectives](week3.md#lesson-gallery-walk)
covered in this lesson.

This class is used for the second teamwork observation assessment.

### Teaching strategies

Cooperative learning - next phase of gallery walk.

Responding to feedback. 25 mins.

Begin the class by communicating to the students that:
- the teacher will observe each group and conduct the second [teamwork assessment](assessment.md#teamwork)
- the two goals of the lesson are to:
  - analyse and respond to the feedback from the gallery walk, updating their prototypes
  - and to reflect on the personas they created and refine their problem statements

Demonstrate to students how to respond to feedback using the feedback response template.
They must:
- discuss the feedback together
- decide if the critique is fair and valid and write a reponse
- identify and write what action they will take

Commence teamwork observation assessment.

```{eval-rst}
:download:`teamwork observation assessment sheet <files/teamwork-observation-sessions.docx>`
```

Each team may wish to split into two smaller groups - each group will respond to the feedback
from one of the other teams. Teams then reform and discuss the feedback and how they'll
improve their prototypes.

```{eval-rst}
:download:`prototype feedback response <files/prototype-feedback-response.docx>`
```

Update personas and redefine the problem. 10 mins.

As a team, students use the feedback to update their personas and 
refine the problem definition.

End the lesson. 5 mins.

Invite teams to comment on one aspect of their prototype that has
now been strengthened because of the feedback from the class.

### Resources

Students are guided by the following template:

```{eval-rst}
:download:`prototype feedback response <files/prototype-feedback-response.docx>`
```

### Assessment

This class is used for the second teamwork observation assessment. See [teamwork assessment](assessment.md#teamwork).


```{eval-rst}
:download:`teamwork observation assessment sheet <files/teamwork-observation-sessions.docx>`
```

## Lesson: Personal reflection and content review

Number of classes: 2

Class 1: personal reflection 2 (second assessable reflection)

Class 2: group reflection

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| Design, produce and evaluate:<br>-evaluation criteria<br>-communication techniques<br>-methods of evaluation | -reflect on personal progress<br>-reflect on personal contributions<br>-reflect on team dynamics and performance<br>-identify and document areas for team improvement |

### Objectives

By the end of this lesson students will know:
- how to evaluate their contributions to the team
- how to respectfully identify team issues

Students can:
- assess their own performance and identify areas for improvement
- assess their team's performance and agree on and document areas for the group to improve


### Teaching strategies

Project work. Explicit scaffolded instruction.

**Class 1**

Students complete their second personal reflection. This includes an
examination of their contributions to developing the prototype.

```{eval-rst}
:download:`personal reflections 2 <files/personal-reflections-2.docx>`
```

They collate their ideas into their folios and submit for assessment.

The next class will be about reflecting on group work.

**Class 2**

Start the lesson with a discussion on negative team roles, how they
affect teamwork, and what we can do about them.

Watch: [negative roles](https://www.youtube.com/watch?v=sQJdsU1HUx8)

Discuss each of the roles with the class and how they could modify their responses
to be constructive and kind.

Take 3 minutes to briefly reflect on your own contributions to the teamwork thus far.
You will not be asked to share these feelings. It's just an opportunity for
quiet contemplation. Do you think you have behaved as one or more of the following:
- the aggressor
- dominator
- jokestar
- withdrawer
- blocker

It's ok if you have. And sometimes we all slip into a negative role. The important
thing is to recognise when we do it and try to control it. We are all continually
improving.

In your team, take 5 minutes to brainstorm ideas on what you can do to safely and respectfully tell someone
when they are being negative and not constructively contributing.

Teams now use the self-assessment template to assess the group's processes and identify issues and improvements.
- Part one is done individually
- Part 2 is for listening, each person gets uninterrupted time to raise issues
- Part 3 is for discussion to identify how issues can be rectified

```{eval-rst}
:download:`evaluating your teamwork <files/assessing-your-teamwork.docx>`
```

Finish the class by inviting students to comment on how they thought this session went.
How will it help them? Was it worthwhile?

### Resources

[Negative roles](https://www.youtube.com/watch?v=sQJdsU1HUx8)

```{eval-rst}
:download:`personal reflections 2 <files/personal-reflections-2.docx>`
```

```{eval-rst}
:download:`evaluating your teamwork <files/assessing-your-teamwork.docx>`
```

### Assessment

The first class in this lesson is the second personal reflection and to start collating
individual contributions to cycle 1 of the project.
See [personal reflections](assessment.md#personal-reflections).

Students will finish collating their designs into a portfolio as part of their homework.
This will be submitted next lesson.


## Lesson: technical considerations - HTTP

Cycle 2. Ideate.

Over the next three lessons students are introduced to technical aspects of the
Internet and website development. This extra depth of knowledge helps students
consider technical issues when exploring ideas for converting their prototypes
into websites. The first is a lesson on the hypertext transfer protocol.

Number of classes: 1

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| Data handling<br>-data types<br>-data transmission | -describe client-server interactions<br>-HTTP request/response<br>-stateless sessions<br>-cookies<br>-HTML<br>-text<br> |

### Objectives

By the end of this lesson students will know:
- the client-server architecture with respect to websites
- IP addresses and URLs
- the basics of the hypertext transfer protocol
- http request and response headers
- http response types

Students can:
- communicate the basics of HTTP REQUEST/RESPONSE protocol
- identify components of HTTP headers
- list http request types
- describe stateless protocols and sessions that use cookies
- use tools to inspect http headers
- list the different types of content that can be returned in a http response body

### Teaching strategies

Explicit scaffolded instruction.

The depth of understanding of the hypertext transfer protocol could be quite
variable across the classroom. So we'll take a diagnostics approach at the
start to guage the level of knowledge across the classroom.

Start with an interesting activity. Ask for a team to come forward and volunteer.
Give them the task: they are to act-out what happens on the Internet when
a person clicks a link on a webpage. If they are struggling to get started, prompt them
to think that computers have to talk to each other over long distances.
What might that look like?

Ask the rest of the class to comment on what they know is happening in the
background when they connect to a website or a social media site. Note
their responses on the board.

Explain the lesson. They will have 25 minutes to research questions about HTTP
on a worksheet. At the end of the lesson each team will have five minutes to plan
a one-minute dramatisation of the message passing that occurs to allow the website to be 
displayed in the browser. Each team will then have one minute to perform their act.
After each act the teacher will write key points on the
board that the act captured. This will form a summary that students use to complete
their worksheets.

### Resources

```{eval-rst}
:download:`HTTP worksheet <files/http-worksheet.docx>`
```

- Commence by watching: [What is http video](https://www.linkedin.com/learning/http-essential-training/what-is-http)
- Suggested reading for research (teams should split the reading across them):
  - [How the Internet works](https://www.freecodecamp.org/news/how-the-internet-works/)
  - [The hypertext transfer protocol](https://www.freecodecamp.org/news/http-and-everything-you-need-to-know-about-it/)
- [live http headers add on for chrome](https://chrome.google.com/webstore/detail/live-http-headers/ianhploojoffmpcpilhgpacbeaifanid/related?hl=en)

### Assessment

Students submit their design portfolios. See [portfolio assessment](assessment.md#design-portfolios).
