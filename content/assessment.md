# Assessment

This assessment page describes the items that students will be assessed on and the grading
method and criteria. A total of 100 marks can be earned.

## Rubric

Refer to the rubric for the assessment criteria for each assessment.

```{eval-rst}
:download:`assessment rubric <files/web-development-rubric.docx>`
```

## Individual assessment

50 marks:
- design portfolio: 15 marks
- personal reflections: 10 marks
- website and/or project coordinator elements: 25 marks

### Design portfolios

Value: 25 marks

Due: last class of week 4

**Description of activity**

Students develop a portfolio of ideas that help them design their websites. The portfolio must
contain a minimum of one idea from each of the following stages in cycle 1 of the project:
- empathise
- define the problem
- ideate
- prototype

Each idea must be accompanied with a one paragraph explanation of how it meets the user's or client's needs.


**Context**

In cycle one of the project students examine the requirements of the project from the user and client
perspectives. This gives the website development task a clear purpose and direction. They must
demonstrate engagement in a genuinue process of understanding the problem from the user's perspective
and a clear justification for their ideas and their contribution to the prototype.

**Outcomes**

5.2.2: designs, produces and evaluates appropriate solutions to a range of challenging problems.


### Personal reflections

Value: 10 marks

Due as follows:
- at the start of week 3
- during week 4
- at the end of week 6
- at the end of week 10 

**Description of activity**

Students are given an opportunity during the project to reflect on their knowledge,
progress, confidence, and contributions to team work. These are guided self-reflections
based on worksheets.

**Context**

Self-reflection and assessment is important to remain on track and identify areas
for improvement. The feedback in these reflections also provides formative assessment
for the teacher to modify or intervene in future lessons.

**Outcomes**

5.2.3: critically analyse decision-making processes in a range of information and software solutions.

5.5.3: describes and compares key roles and responsibilities of people in the field of information and software technology.


### Website and/or project coordinator elements

Value: 25 marks

Due: week 9

**Description of activity**

Students create 2-3 elements that contribute to the website development. These can be:
- records of project meetings (project coordinator only)
- website content-designer elements
- website developer elements

In a four minute interview with the teacher, students describe up to three elements.
For students who also take on the additional role of project coordinator, they must
demonstrate their knowledge of how the project planning elements helped the team address issues and
kept the project on track. For the content designers and website developers, they 
they must demonstrate their knowledge of how these contribute to good website design.

The quality and quantity of elements produced, along with teacher observations during
weeks 7 and 8, will be used to assess the level of competency the student has achieved
in using the software.

Project coordinators will:
- keep meeting records: the agenda, decisions made, and issues raised in the agile meetings
- track issues in a tracking sheet, actions to be taken, and the resolution
- ensure the kanban board is kept up to date
- schedule meetings

Records are to be kept using these templates:

```{eval-rst}
:download:`meeting record <files/team-meeting-decision-log.docx>`
```

```{eval-rst}
:download:`issues log <files/issues-log.docx>`
```

Examples elements that website content designers can create:
- digital versions of the character design
- storyboard
- the webpage background
- logos
- button designs
- animations

Example elements that Web developers may create:
- the page layout
- organisation of content
- considerations for publication
- dynamic website elements
- custom HTML and CSS elements

**Context**

For the team to be successful in creating the website, they must negotiate the roles each
member will take.
These are project coordinator, website designer and/or website developer. In their roles they
create project elements that contribute to the final website. The person who takes on
the coordination role will also take on one of the other roles.

**Outcomes**

5.1.1: selects and justifies the application of appropriate software programs to a range of tasks.

5.2.2: designs, produces and evaluates appropriate solutions to a range of challenging problems.

5.5.2: communicates ideas, processes and solutions to a targeted audience.

## Team assessment

Each team will receive a mark out of 50.

The team mark is used as the basis for each student's individual teamwork mark as described in
{ref}`marks-for-teamwork`.

- website presentation: 30 marks
- teamwork: 20 marks

### Website presentation

Value: 30 marks

Due: week 10

- website design considerations: 15 marks
- website layout: 10 marks
- website publishing and discovery considerations: 5 marks

**Description of activity**

During week 9 students complete the website and decide on talking points
for the following week's presentation. These must address the assessment
criteria defined in the [rubric](#rubric). They present their websites to the class
and receive final feedback.

**Context**

This project culminates in the creation and publication of a website.
Students must present this website to their client (the class), justifying
their choice of design with respect to how it addresses the client's needs.

**Outcomes**

5.2.2: designs, produces and evaluates appropropriate solutions to a range of problems.

5.2.3: critically analyses decision-making processes in a range of information and software solutions.

5.5.2: communicates ideas, processes and solutions to a targeted audience.

### Teamwork

Value: 20 marks

Due as follows:
- team observations in week 2: identifying content and design elements (5 marks)
- team observations in week 4: improving the website design (5 marks)
- team observations in week 6: role negotiation and planning (5 marks)
- hold team scrums in weeks 7-9 to monitor project progress (5 marks)


**Description of activity**

Several team planning activities are held throughout the project.

In the first three activities, students use the majority of a class to discuss
and negotiate the website design, its content and assign the roles and responsibilities
to create the website. The teacher observes these interactions and assigns
each team a mark of 5 based on constructive and respectful conversations and recording
key points and decisions.

In weeks 7-9 students self-organise and document outcomes from several
scrum meetings. The assessment is based on the evidence of records
from these meetings.

**Context**

Team meetings are important for the exchange, development and negotation of ideas.
They're used in this project to explore and shape the form of the final website,
and to keep the website build on track.

**Outcomes**

5.5.1: applies collaborative work practices to achieve tasks.

5.5.3: describes and compares key roles and responsibilities of people in the field of information and software technology.


(marks-for-teamwork)=
## Student marks for teamwork

The mark out of 50 for teamwork is used as the basis for each individual's
teamwork mark. It is modified by an adjustment factor that is based on
the relative contribution of each team member.

The teams determine the relative contribution of each team member
through negotiation. What follows is an example to explain its calculation.

17 points are available across three areas. Students must split these
across the team members. Each team member is given a whole
number (no fractions of a mark can be assigned).
No team member can score more than 5. For example, for a team of 5:

| Area | Student A | Student B | Student C | Student D | Student E | Total |
|------|-----------|-----------|-----------|-----------|-----------|-------|
| Engagement and conduct in meetings | 4 | 2 | 5 | 3 | 3 | 17 |
| Contribution to the development of ideas | 4 | 5 | 1 | 3 | 4 | 17 |
| Effort in creating website elements and/or project coordination | 4 | 4 | 2 | 4 | 3 | 17 |
| Student total | 12 | 11 | 8 | 10 | 10 | 51 |

The average relative contribution each student can make to the project is 10.2 (51/5).
Each student's contribtion relative to the average is therefore:

relative contribution = student total / 10.2

So student A's relative contribution is: 12 / 10.2 = 1.176

Student C's relative contribution is: 8 / 10.2 = 0.784

If the team's score is 35 out of 50, it means that 15 marks were not awarded.
These 15 marks are weighted by the relative contributions and used to adjust
the scores.

Student A's score will be higher than the team score of 35 by 15*(1.176-1) = approx 2.5

Student B's score will be lower than the team score of 35 15*(1-0.784) = approx 3

So student A's final score for teamwork is 37.5, and student B's is 32.

Note that the higher the overall team score, the less impact the relative contributions
have on the final score. This reflects an assumption that in a high-performing
team a low relative contribution is still significant in order for the team to 
achieve such a high score.

A spreadsheet is used to perform the calculations:

```{eval-rst}
:download:`teamwork mark spreadsheet <files/teamwork-mark.xlsx>`
```
