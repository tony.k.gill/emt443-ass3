# Week 5

Cycle 2 ideate.

By the end of this week students will have a good grasp of the
technical elements required to build a website. They'll use that
knowledge to identify how they will can create the web pages and content
described by their prototypes. The week will end by exploring webflow,
a web-based application they'll use to build and publish their sites.

## Lesson: technical considerations - website building

Number of classes: 3

- Class 1: HTML and CSS
- Class 2: animations, and static and dynamic generation of pages
- Class 3: linking technical elements to prototypes

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| Software:<br>-types and examples of software<br>-features and elements of a GUI | -HTML and CSS: client-side technologies that underpin web pages<br>-content management systems<br>-interactive web page elements including buttons, lists, menu items and animating them |
| Data handling<br>-data types  | -web pages as text documents<br>-hypertext<br>-graphics<br>-database concepts of collections, items and fields |
| People:<br>-roles and responsibilities | -the role of the web developer |
| Design, produce and evaluate:<br>-defining and analysing the problem<br>-prototyping | -identify technical elements for building the website<br>-refining the prototype with the technical elements to be used |

### Objectives

The intent is to give students a brief introduction and some experience with
HTML and CSS. Strictly speaking, they don't need knowledge of the syntax of
these languages as they will use a visual website builder software, which hides
those details. However, having some extra depth of knowledge is important, and this
lesson introduces them to some key HTML and CSS elements that the web builder software uses.

By the end of this lesson students will know:
- that HTML and CSS are the languages that underpin webpage structure and style
- the basics of HTML tags, elements and attributes
- some key HTML elements used for the layout of web pages
- the CSS box model

Students can:
- use online tutorials to learn about HTML and CSS
- describe the CSS box model
- explain what HTML and CSS elements they could use to lay out their website

### Teaching strategies

Provide students with explicit scaffolded instruction.

**Engage Class 1: HTML and CSS introduction**

10 minutes

- Start with a class discussion, inviting students to comment on what they know
  about how web pages are created. Some may say using software, others may have
  some knowledge of HTML, CSS, javascript, databases and so on.
- Write the student's responses on the board
- explain the goal of today's lesson: to learn about HTML and CSS and begin to
  understand how they can use it to create the web pages for their sites
- watch [intro to html and css](https://university.webflow.com/lesson/intro-to-html-css)
  up to 2:30
- ask students to comment:
  - what does HTML allow you to do?
  - what does CSS allow you to do?
  - what makes CSS powerful?

**Explore: HTML**

20 minutes

- On the [w3schools HTML tutorial](https://www.w3schools.com/html/default.asp):
  - use the **HTML HOME** page to demonstrate changing html to modify the content of a web page and have the students try it themselves
  - use the **HTML Introduction** page to describe tags and elements and have the students try it themselves
  - use the HTML **HTML basic** page to describe a simple HTML page structure and the _html_, _head_, and _body_ tags
  - use the **HTML attributes** page to introduce the link and image elements and their attributes:
    - have the students go to the **HTML links** page and modify the example to create a link that points to
      a website of their choice
  - have students explore other pages to learn about headings, paragraphs, and lists and try examples for themselves
  - ask students to explore [w3schools HTML tutorial](https://www.w3schools.com/html/default.asp)
    and identify which tags can be used to insert images and create lists and tables

Extension: can they use Google Chrome's inspect tool to edit the headings on
the [Ms Marvel site](https://marvelrising.marvelhq.com/characters/ms-marvel)?

**Explore: CSS**

15 minutes

- Demonstrate and have students use the [w3schools CSS intro](https://www.w3schools.com/css/css_intro.asp)
  to highlight the power of CSS; i.e. that you can change an entire website's look and feel and layout
  without changing any content
- Use the **CSS Syntax** page to explore the syntax with students
- Watch the [intro to the CSS box model](https://university.webflow.com/lesson/intro-to-the-box-model)
- Now ask students to look at the **CSS Box model** on [w3schools](https://www.w3schools.com/css/css_intro.asp)
  and try the example
- Can they explain each of these CSS elements in terms of how they affect the box in the box model:
  - width
  - border
  - padding
  - margin 

End of class 1

**Engage Class 2: animations and dynamically created pages**

10 minutes

We start this class by turning our attention to animations.

Engage the class by showing this video, which demonstrates the _histography_ website.
Sound is important as it adds to the experience.
[histography.io video](https://vimeo.com/134567426?embedded=true&source=vimeo_logo&owner=16231814)

Now have the students go to the site and explore it for themselves: [history.io website](http://www.histography.io/)

In a class discussion ask students to identify the features that they liked. Expect them to 
say being able to read a story by clicking on a dot, changing the era, sliding the
time scale, filtering events by theme, the animations, the sounds.

In the first part of this lesson we'll introduce you to animating the content of
your website.

Watch [introduction to animations](https://university.webflow.com/lesson/intro-to-interactions).
Afterwards, emphasise that the two key concepts for students to learn are triggers and animations.
Can someone think of a trigger? What might the resulting animation be?

**Explore - triggers and animations**

10 minutes

In pairs, students have 5 minutes to look at the [history.io website](http://www.histography.io/)
and identify as many triggers as possible. Discuss each trigger with your partner and its animation
and note it on your worksheets.

```{eval-rst}
:download:`triggers and animations worksheet <files/triggers-and-animations.docx>`
```

Have a short class disussion to list the triggers that they identified. 
Finish by stating that animations are created using javascript. However we don't have
to know much about javascript because we use software tools to create the animations visually.

**Static and dynamic web pages**

10 minutes.

Introduce students to the notion of creating web pages that are:
- static - once created they must be edited manually to update them
- dynamic - create web page templates and populate them with content that is stored in a database (or content management system, CMS)

Two short (2 min each) videos will help explain the concepts:
- [an overview of dynamic content](https://university.webflow.com/lesson/intro-to-dynamic-content)
- [intro to a CMS](https://university.webflow.com/lesson/intro-to-webflow-cms)

Invite students to comment on the differences between static and dynamic web pages.
Ask some probing questions:
- when are they useful?
- Give an example of a collection
- What is an example of a collection? They might identify 'people'
- What does each item represent? If the website is about a company each item could be a staff member
- What are its fields? name, position, photograph, biography
- So a company will have a database with a collection of staff and their details
- Could we build web pages from this dynamically? Let's explore. 

**Explore: analysing a website**

10 minutes.

In your pairs, complete the supplied worksheet by exploring the 
the [Barpa website](https://barpa.com.au/), an Indigenous-owned construction company,
tackling many large projects across Australia.

Students will:
- identify one animation
- identify one or two collections
- understand what each element in the collection represents
- list fields for each item

```{eval-rst}
:download:`dynamic webpages <files/dynamic-webpages.docx>`
```

**Summarise and connect**

5 minutes

Class discussion:
- list the collections that were identified and what do they represent?
  - expect to hear at least 'staff' and 'projects'
- list the items: expect to hear people's names, or project names
- list the possible fields that were identified for the items:
  - a person's name, their position, a brief biography, their location, photograph
  - project name, description, location, cost, photograph

Finish with questions for them to think about:
- would you include animations on your site?
- do you think static or dynamic content generation would be suitable for your site?

End of class.

**Class 3 - linking technical elements to prototypes**

15 minutes. Group review and introduction.
 
Review. Over the last two classes you have learned:
- HTML for holding content
- CSS for styling that content
- triggers and animations for creating engaging, interactive experiences
- and dynamically generating web pages based on content stored in databases or content management systems

Today you will start to look out integrating these elements to create your web pages.

As a class, choose one of the team's prototypes. Choose one of the pages, and ask them to:
- identify a section of the page
- how could you use HTML and CSS to create it?
- what triggers and animations could you add, if any?
- would this site benefit from dynamic content? Why or why not?

Annotate the protoype as you go - with pencil or sticky notes so it can be removed if the team wants to.
Use vocabulary from the previous lessons. For example: use a HTML section or div; use the CSS
box model to space and arrange elements; add an animation to the image when the mouse hovers over it.

**Explore - prototypes**

Part 1. 15 minutes

In their teams, students are to work together to break each of their web pages down
into sections. They can allocate each page to a pair to work on.
- They must label each section and its purpose
- Start to determine the relative spacing between the elements (e.g. small, medium, large)
- Label the content that they will animate, what will trigger the animation and
  what will the animation be?
- What else might they consider? e.g.
  - backgrounds
  - sounds

The teacher moves between teams and asks them to describe how the technical elements
meet the needs of their client, the target user, and how it will help create an engaging
experience for visitors to their site.

Part 2. 5 minutes

The teams come back together and share their ideas with each other.
Ask them to identify one element to discuss with the class and to assign a spokesperson.

**Summarize and connect**

10 minutes

Ask a representative from each team to identify one element on their
prototype and share
with the class how they think they'll implement it.

In the next lesson we'll start to look at software packages that you can use to 
create your website's content and build and publish your sites.


### Resources

- [intro to html and css](https://university.webflow.com/lesson/intro-to-html-css)
- [intro to the CSS box model](https://university.webflow.com/lesson/intro-to-the-box-model)
- [w3schools HTML tutorial](https://www.w3schools.com/html/default.asp)
- [w3schools CSS tutorial](https://www.w3schools.com/css/css_intro.asp)
- [Ms Marvel site](https://marvelrising.marvelhq.com/characters/ms-marvel)
- [histography.io video](https://vimeo.com/134567426?embedded=true&source=vimeo_logo&owner=16231814)
- [introduction to animations](https://university.webflow.com/lesson/intro-to-interactions)
- [history.io website](http://www.histography.io/)
- [an overview of dynamic content](https://university.webflow.com/lesson/intro-to-dynamic-content)
- [intro to a CMS](https://university.webflow.com/lesson/intro-to-webflow-cms)
- [Barpa website](https://barpa.com.au/)
- the prototypes developed in previous lessons, markers, post-it notes, pencils

```{eval-rst}
:download:`triggers and animations worksheet <files/triggers-and-animations.docx>`
```

```{eval-rst}
:download:`dynamic webpages <files/dynamic-webpages.docx>`
```

## Lesson: software evaluation

Number of classes: 2

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| Software:<br>-the purpose of a software system | -understand the purpose of graphic design and website builder software<br>-define design system in terms of its features and tools  |
| Data Handling:<br>-bits and bytes such as kilobytes, megabytes, gigabytes and terabytes | examine and understand the limits on data storage provided by developers of web-based applications |
| Issues:<br>-Legal: copyright and licensing | Students examine terms and conditions on use of stock images and copyright retention of uploaded data |
| Design, produce and evaluate:<br>-designing possible solutions | Students consider how they can use technology to produce their websites, going from design to implementation. |
| Past, current and emerging technologies | Learn how to build and create websites with dynamic content using the next generation of web-based applications without having to write code, thus boosting productivity |


### Objectives

By the end of this lesson students will know:
- the software tools they'll use to create website content
- the software tools they'll use to create and publish websites
- how to evaluate the features of software
- the differences between raster and vector graphics

Students can:
- evaluate and communicate the tools and features of software packages
- identify the tools of a software package that will help them create their website content or build and publish their website
- create learning resources to share with the class


### Teaching strategies

Cooperative learning: jigsaw.

This lesson uses the next two classes (this week and next). In class one:
- introduce the students to the tools they will use to create their content and website
- discuss roles and responsibilities of web development teams
- use a Jigsaw approach, creating specialist groups to learn the tools and features of the software package for creating the website
- students do research on their chosen software package and collate learning information to share

In class two:
- students complete their research
- specialist groups create a shared resource that describes the tools and features of a package
- specialists describe to their teams how they can use the tools of the software packages to create
  the website or its content

**Class 1**

_Engage. 20 minutes._

Explain to students that in this lesson, over the next three classes, they'll
begin to learn the software they will use to create their websites. We'll look at
three packages. Two of these are for content design (you can choose to use one or both),
and one is for building and publishing the website.

After watching the introductory videos on each package, in your teams you'll assign
specialists to learn the features of each one. So have in mind whether you prefer to design
and create content or build websites.

Watch the following: 
- [gravit designer](https://www.designer.io/en/offers/freegravit/)
- [inkscape](https://www.youtube.com/watch?v=pa6a7oz7vEE)
- [webflow overview](https://www.youtube.com/watch?v=o0KmjcGd6jw)
- [intro to the webflow designer](https://www.youtube.com/watch?v=O5TdnuUhIgs&t=8s)

Discuss:
- roles: difference between graphic designer (or content creators) and web developers
- what's the difference between vector raster graphics, and the advantages
  and disadvantages of each

_Team huddle. 2-3 minutes._

- teams assign members to specialist groups
- a maximum of two people from your group can be assigned to one specialty
- there ought to be an even distribution of students across the groups
- create specialist groups of size 3-5

_Specialist group research. Remainder of class._

- students join their specialist groups at another table
- they decide who will research features, tools, technical and legal considerations
- each person conducts independent research
- in assessing the features and tools they may:
  - read online tutorials
  - watch online video tutorials - those produced by the software developers are suggested
  - use the software's help
  - experiment with the tool itself - but don't get bogged down on creating your designs!
- in the worksheet, they note the feature or tool, describe it and suggest how it could
  help them build their website (how it helps create the content or layout the website)
  used to create content
- they also provide the location of the resources they used to learn that feature
- any known technical limitations of the software, such as data storage
  limits offered by developers of the web-based applications, especially on the free accounts
- note and consider legal issues in the worksheet:
  - for example, use of stock images in templates
  - who retains copyright of uploaded data
- the goal is for each student to note three tools or features or technical/legal issues
  in today's and the next class

```{eval-rst}
:download:`software features worksheet <files/software-features-worksheet.docx>`
```

End of class

### Resources

Ensure the following software packages are installed on the students'
or computer room's computers:
- [gravit designer](https://www.designer.io/en/offers/freegravit/)
- [inkscape](https://www.youtube.com/watch?v=pa6a7oz7vEE)

Videos:
- [webflow overview](https://www.youtube.com/watch?v=o0KmjcGd6jw)
- [intro to the webflow designer](https://www.youtube.com/watch?v=O5TdnuUhIgs&t=8s)

```{eval-rst}
:download:`software features worksheet <files/software-features-worksheet.docx>`
```
