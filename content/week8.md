# Week 8

Cycle 2 build.

## Lesson: website build sprint 2

Number of classes: 4

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| People:<br>-roles and responsibilities | -the role of the web developer, project coordinator, content designers |
| Design, produce and evaluate:<br>-producing solutions<br>-management<br>-communication techniques | -creating a website<br>-tracking project progress through meetings, record keeping and kanban boards |

### Objectives

By the end of this week students will further build their knowledge in:
- processes for monitoring project progress
- tools for creating and publishing websites

Students can:
- use software to create websites and website content
- publish a website
- track project progress
- identify issues and suggest solutions


### Teaching strategies

Mostly project work, except class 1 where students are directed in how to 
evaluate progress and plan sprint 2 using their kanban board.

In class 1 have a class discussion about planning sprint 2. The team must:
- have a scrum
- identify incomplete tasks from sprint 1 and carry these forward to sprint 2
- reevaluate their performance and decide:
  - reduce, increase, or keep unchanged the number of tasks for sprint 2
  - if they are behind, think about what tasks are a lower priority and can be discarded
  - if they are tracking well, consider what additional tasks would be valuable and achievable
  - what other tactics could be used to ensure the project is completed on time, think in terms of:
    - time (more after-class hours?)
    - quality (change and manage expectations)
    - scope (reduce size of tasks)
    - people (reassign tasks, improve communication)

Remind students to:
- engage in scrums to identify and solve issues
- scrum master to maintain documentation
- reflect on and document task progress
- keep the kanban up to date
- publish the second draft of your site online by the end of the week
- refer to the [rubric](assessment.md#rubric) and use the assessment requirements to help with decision making
- circulate between the groups through the week, providing feedback and guidance, just-in-time teaching, and keeping students on track

### Assessment

Teacher makes observations about students' competencies using the software packages.
See [assessment for website and/or project coordinator elements](assessment.md#website-andor-project-coordinator-elements).
