# Week 9

Cycle 2 build.

## Lesson: website build sprint 3

Number of classes: 4

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| People:<br>-roles and responsibilities | -the role of the web developer, project coordinator, content designers |
| Design, produce and evaluate:<br>-producing solutions<br>-management<br>-communication techniques | -creating a website<br>-tracking project progress through meetings, record keeping and kanban boards |

### Objectives

By the end of this week students will further build their knowledge in:
- processes for monitoring project progress
- tools for creating and publishing websites

Students can:
- use software to create websites and website content
- publish a website
- track project progress
- identify issues and suggest solutions
- prepare key talking points for a client meeting (class presentation)


### Teaching strategies

Project work.

Start the week with a class discussion. Students will plan sprint 3.
They must include tasks that help the team prepare for the client presentation
next week. The presentation involves using the website to show the client
how it will convince the show's creators to include their character
in the next release. The points that must be covered are detailed in the
[rubric](assessment.md#rubric). Teams each get 10 minutes to present, followed
by three minutes of client (class) questions and feedback. The ten minutes
should be spent as follows:
- website design considerations: 4-5 mins
- website layout: 3-4 mins
- website publishing and discovery: 1-2 mins

Teams must be ready to present from the first class next week. The client
could call on them at any time.

Remind students to:
- engage in scrums to identify and solve issues
- scrum master to maintain documentation
- reflect on and document task progress
- keep the kanban up to date
- publish the **third and final** version of your site online by the end of the week
- refer to the [rubric](assessment.md#rubric) and use the assessment requirements to help with decision making

Circulate between the groups through the week, providing feedback and guidance, just-in-time teaching, and keeping students on track.

### Assessment

Use this week's classes to conduct student interviews.
See [assessment for website and/or project coordinator elements](assessment.md#website-andor-project-coordinator-elements).

Each interview is 4 minutes. In choosing the order of the interviews, use the
kanban boards to identify those students who have commenced work on their third task
(or second task for project coordinators). These students are better prepared to
go first and provides an opportunity for others to complete additional tasks
and demonstrate further knowledge.

Competencies in using software can be gauged from the quantity and
quality of the work presented during the interviews and through previous
classroom observations.

At any one time three students will be engaged in the interview or preparing for it.
The process is:
1. 10 minutes prior to the interview students:
    1. are provided with the list of interview questions
    2. they open the work/files that they think will help them best answer those questions
2. the interview is conducted (4 mins)
