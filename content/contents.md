# Project overview

```{eval-rst}
.. toctree::

   self
   index.md
   assessment.md
   week1.md
   week2.md
   week3.md
   week4.md
   week5.md
   week6.md
   week7.md
   week8.md
   week9.md
   week10.md
   resources.md
   section-b.md
```
