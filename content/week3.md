# Week 3

Cycle 1. Prototype and test

A personal reflection is used to commence the lessons for this week.

By the end of this week students will have created a prototype of their website, justifying
their choices of content and layout with respect to the criteria for presenting their
websites in the final assessment. They will have provided feedback to two other team's prototypes.


## Lesson: prototyping

Number of classes: 3

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| Design, produce and evaluate:<br>-designing possible solutions | -prototyping<br>-evaluating the effectiveness of a design for solving user needs |

### Objectives

By the end of this lesson students will know:
- how to create a wireframe prototype
- the content and design requirements of the website

Students can:
- use pen and paper to create a wireframe prototype of their website
- discuss and negotiate website content and layout ideas
- assess requirements when designing websites
- provide sound reasons for design decisions to a target audience using images and writing


### Teaching strategies

Explicit scaffolded instruction and project work.

**Class 1**

Start with general feedback on the team observations made at the last lesson.

Students then do their first personal reflection. Students must reflect on their and the team's performance,
what skills they think they have that will help the team, and how well they think
they can use those skills. These are assessable. Meaningful responses to be provided.

```{eval-rst}
:download:`personal reflection 1 <files/personal-reflections-1.docx>`
```

Open the next lesson by stating that website designers and developers always create
quick and cheap prototypes of their websites to develop ideas and get feedback
from their clients and users. They do this so they don't spend a lot of time
and money developing something the client doesn't want or doesn't address the
user's needs.

- As a class watch the video on [creating templates with wireframing](https://www.flux-academy.com/blog/20-wireframe-examples-for-web-design)
- State that:
  - the lesson goal is to create a wireframe prototype using pen and paper by the end of class 3
  - the prototypes will be critiqued by other teams in class 4 of this week, using criteria directly
    related to the assessment of the website presentation to be completed in week 10
  - discuss the [gallery walk questions](#resources)
  - show how the questions relate to the [assessment rubric](assessment.md#rubric)
- Discuss the exemplar wireframes on the
  [creating templates with wireframing](https://www.flux-academy.com/blog/20-wireframe-examples-for-web-design)
  website
- Lead a class brainstorming session (using knowledge gained from last week) on:
  - how to position your website with respect to other content you might put on social media platforms
  - types of websites
  - content of websites and reasons for choosing them
  - layout of webpages and reasons for choices


**Class 2**

- Engage the class with a [crazy 8's exercise](https://designsprintkit.withgoogle.com/methodology/phase3-sketch/crazy-8s)
  where everyone generates eight webpage layout ideas in 8 minutes
- Students work in their teams, sharing their crazy 8 ideas, and plan their prototypes, assigning one or
  two tasks to each person
- they commence creating their website prototypes;
  they use the [gallery walk questions](#resources) to guide them


**Class 3**

Students complete their prototypes.

### Assessment

In class 1 students complete personal reflections 1.
See [personal reflections](assessment.md#personal-reflections).

```{eval-rst}
:download:`personal reflection 1 <files/personal-reflections-1.docx>`
```

### Resources

- [Creating templates with wireframing](https://www.flux-academy.com/blog/20-wireframe-examples-for-web-design)
- butchers paper
- pens, markers, pencils, crayons, ...
- A3 graph pads
- post-it notes

```{eval-rst}
:download:`personal reflection 1 <files/personal-reflections-1.docx>`
```

```{eval-rst}
:download:`gallery walk questions <files/gallery-walk-questions.docx>`
```

## Lesson: gallery walk

Number of classes: 2

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| Design, produce and evaluate:<br>-designing possible solutions<br>-evaluation criteria | -prototyping<br>-evaluating the effectiveness of a design for solving user needs<br>-reflect on feedback to alter solution |

### Objectives

By the end of this lesson students will know:
- how to provide feedback using assessment criteria
- the value of early feedback in improving designs
- how their websites can be improved
- how to document responses to feedback

Students can:
- discuss and negotiate website content and layout ideas
- assess requirements when designing websites
- provide constructive feedback to others using assessment criteria
- discuss and critically analyse feedback and use it to:
  - identify weaknesses in their persona designs
  - refine their problem definitions
  - improve their website designs

### Teaching strategies

Cooperative learning. Students provide feedback on other teams' prototypes in a gallery walk.

**Class 1**

Engage. Watch [gallery walk](https://www.theteachertoolkit.com/index.php/tool/gallery-walk).

Describe what will happen:
  - students will provide constructive feedback on other team's prototypes, including:
    - personas
    - problem definition
    - and prototype
  - the prototypes will be arranged at each team's table
  - they will use a list of gallery walk questions to guide them in
    critiquing the work
  - remember to be kind and thoughtful and also constructive
  - do not take offence in receiving feedback, as the purpose of it
    is to improve everyone's work

```{eval-rst}
:download:`gallery walk questions <files/gallery-walk-questions.docx>`
```

Each team displays their personas, problem definition, and prototypes on their table. Teams then
rotate one table and discuss and provide a written critique on the other team's work. Two rotations are done, so
teams receive feedback from two different teams.

Allow 15 minutes per prototype.

The teacher circulates the room, guiding students in what constitutes constructive feedback.

Teams will reflect on the feedback next lesson.

### Resources

```{eval-rst}
:download:`gallery walk questions <files/gallery-walk-questions.docx>`
```
