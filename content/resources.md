# Project resources

A complete list of resources used across the project.

The resources for each lesson are also given in the lesson plans.

- [rubric](assessment.md#rubric)
- [High quality example website](https://www.karliekloss.com/)
- [Low quality example website](https://marvelrising.marvelhq.com/characters/ms-marvel)
- [Five stages of design thinking](https://www.interaction-design.org/literature/article/5-stages-in-the-design-thinking-process)
- [Stan Lee interview](https://www.youtube.com/watch?v=AhCyVP5bTkQ)
- [Airport design video](https://www.interaction-design.org/literature/topics/empathy)
- [Point of view problem statement](http://tualatinpsychology.weebly.com/uploads/5/7/2/8/5728202/point-of-view-problem-statement.pdf)
- [the history of website design](https://www.smamarketing.net/blog/the-history-of-website-design)
- [five great website designs](https://www.youtube.com/watch?v=AmHEfTSBXiY)
- [the importance of great visual design](https://cxl.com/blog/first-impressions-matter-the-importance-of-great-visual-design/)
- [what is a site map](https://slickplan.com/sitemap/what-is-a-site-map) 
- [roles and responsibilities in website development](https://www.websiteplanet.com/blog/web-designer-vs-web-developer/)
- [karliekcloss.com](https://www.karliekloss.com/)
- [The Big Bang Theory on teamwork](https://www.youtube.com/watch?v=8Amu3UBj-qw)
- [Creating templates with wireframing](https://www.flux-academy.com/blog/20-wireframe-examples-for-web-design)
- [gallery walk](https://www.theteachertoolkit.com/index.php/tool/gallery-walk)
- [Negative roles](https://www.youtube.com/watch?v=sQJdsU1HUx8)
- [What is http video](https://www.linkedin.com/learning/http-essential-training/what-is-http)
- [How the Internet works](https://www.freecodecamp.org/news/how-the-internet-works/)
- [The hypertext transfer protocol](https://www.freecodecamp.org/news/http-and-everything-you-need-to-know-about-it/)
- [live http headers add on for chrome](https://chrome.google.com/webstore/detail/live-http-headers/ianhploojoffmpcpilhgpacbeaifanid/related?hl=en)
- [intro to html and css](https://university.webflow.com/lesson/intro-to-html-css)
- [intro to the CSS box model](https://university.webflow.com/lesson/intro-to-the-box-model)
- [w3schools HTML tutorial](https://www.w3schools.com/html/default.asp)
- [w3schools CSS tutorial](https://www.w3schools.com/css/css_intro.asp)
- [Ms Marvel site](https://marvelrising.marvelhq.com/characters/ms-marvel)
- [histography.io video](https://vimeo.com/134567426?embedded=true&source=vimeo_logo&owner=16231814)
- [introduction to animations](https://university.webflow.com/lesson/intro-to-interactions)
- [history.io website](http://www.histography.io/)
- [an overview of dynamic content](https://university.webflow.com/lesson/intro-to-dynamic-content)
- [intro to a CMS](https://university.webflow.com/lesson/intro-to-webflow-cms)
- [Barpa website](https://barpa.com.au/)
- [gravit designer](https://www.designer.io/en/offers/freegravit/)
- [inkscape](https://www.youtube.com/watch?v=pa6a7oz7vEE)
- [webflow overview](https://www.youtube.com/watch?v=o0KmjcGd6jw)
- [intro to the webflow designer](https://www.youtube.com/watch?v=O5TdnuUhIgs&t=8s)
- [musical teamwork](https://www.youtube.com/watch?v=qh4ms4xgHko)
- [intro to kanban](https://www.youtube.com/watch?v=R8dYLbJiTUE) 
- [intro to scrum](https://www.youtube.com/channel/UCqYarI0avGozOjvpN3y1l7A)


```{eval-rst}
:download:`scope and sequence plan <files/scope-and-sequence.docx>`
```

```{eval-rst}
:download:`skills matrix <files/skills-matrix-template.xlsx>`
```

```{eval-rst}
:download:`Stan Lee focus questions <files/stan-lee-research-q.pptx>`
```

```{eval-rst}
:download:`persona development template <files/persona-development.docx>`
```

```{eval-rst}
:download:`WWW considerations team worksheet <files/www-considerations-specialist-q.docx>`
```

```{eval-rst}
:download:`WWW considerations team worksheet <files/www-considerations-team-q.docx>`
```

```{eval-rst}
:download:`meeting record <files/team-meeting-decision-log.docx>`
```

```{eval-rst}
:download:`personal reflections 1 <files/personal-reflections-1.docx>`
```

```{eval-rst}
:download:`personal reflections 2 <files/personal-reflections-2.docx>`
```

```{eval-rst}
:download:`personal reflections 3 <files/personal-reflections-3.docx>`
```

```{eval-rst}
:download:`personal reflections 4 <files/personal-reflections-4.docx>`
```

```{eval-rst}
:download:`prototype feedback response <files/prototype-feedback-response.docx>`
```

```{eval-rst}
:download:`gallery walk questions <files/gallery-walk-questions.docx>`
```

```{eval-rst}
:download:`evaluating your teamwork <files/assessing-your-teamwork.docx>`
```

```{eval-rst}
:download:`teamwork observation assessment sheet <files/teamwork-observation-sessions.docx>`
```

```{eval-rst}
:download:`HTTP worksheet <files/http-worksheet.docx>`
```

```{eval-rst}
:download:`triggers and animations worksheet <files/triggers-and-animations.docx>`
```

```{eval-rst}
:download:`dynamic webpages <files/dynamic-webpages.docx>`
```

```{eval-rst}
:download:`software features worksheet <files/software-features-worksheet.docx>`
```

```{eval-rst}
:download:`kanban planning <files/kanban-planning.pptx>`
```

```{eval-rst}
:download:`issues log <files/issues-log.docx>`
```

```{eval-rst}
:download:`teamwork mark spreadsheet <files/teamwork-mark.xlsx>`
```
