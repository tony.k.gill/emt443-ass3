# Week 1

Cycle 1, Launch and define the problem.

By the end of this week students will understand the project requirements,
their team's skills relevant to the project, the end user's needs,
and have defined the problem.

## Lesson: know your project and team

Number of classes: 2

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| Topic 1. Design, produce and evaluate:<br>-Definining and analysing the problem<br>-Management<br>-Collaboration and group work<br>-Communication techniques: written and verbal | -Project development: processes and techniques<br>-Understand key project management principles<br>-know your people's expertise/strengths<br>-Document skills in skills matrix|


### Objectives

By the end of this lesson students will know:
- the project context and aims
- what they'll learn in this course
- the assessment criteria
- the benchmark for a high-quality website
- the design thinking process (a refresher from project 1)
- the project schedule and their role in refining it

Students can:
- self-assess their skills and strengths
- identify their team members' skills and strengths and document them

### Teaching strategies

Explicit instruction and classroom activities about project expectations.

- Use the [student brief](index.md#student-brief) to introduce the project.
- Have students explore the [Exemplar website](https://www.karliekloss.com/) and discuss in their team:
  - What is your overall impression of this website?
  - What problem does it solve and for who?
  - What do you like?
  - Would a website like it be suitable for profiling your character?
  - What would you improve or change?
- Share with class
- Repeat the exercise with a [lower-quality example](https://marvelrising.marvelhq.com/characters/ms-marvel)
- Discuss the assessments with the [rubric](assessment.md#rubric)
- Recap the [five stages of design thinking](https://www.interaction-design.org/literature/article/5-stages-in-the-design-thinking-process) that students learned and used in project 1 
- Describe what they're expected to learn through a team exercise in identifying skills:
  - working in teams students review the list of skills on the skills matrix
  - for every skill they self-assess their  ability in terms of the confidence levels:
    - you know what to do
    - you know how to do it
    - you can do it
  - students will assess the development of their skills with a series of self-reflections across the project
- Classroom discussion and pep talk: teams reflect on their confidence to complete the project and share with the class 

```{eval-rst}
:download:`skills matrix <files/skills-matrix-template.xlsx>`
```

### Resources

- [](assessment.md#rubric)
- [High quality example website](https://www.karliekloss.com/)
- [Low quality example](https://marvelrising.marvelhq.com/characters/ms-marvel)
- [Five stages of design thinking](https://www.interaction-design.org/literature/article/5-stages-in-the-design-thinking-process) that students learned and used in project 1 

```{eval-rst}
:download:`skills matrix <files/skills-matrix-template.xlsx>`
```


## Lesson: Define the problem

Number of classes: 2

In this lesson students complete the _empathise_ and _define the problem_ phases of the 
design cycle. 

### Syllabus concepts and skills covered

| Core | Option |
|------|--------|
| Design, produce and evaluate:<br>-research<br>-designing possible solutions<br>-defining and analysing the problem | -undertake research on a public figure<br>-create a persona based on a public figure<br>-generate a problem statement |
| Issues:<br>-ethical issues | -assess the accuracy, validity and bias of information about a public figure |

### Objectives

By the end of this lesson students will know:
- how to evaluate the needs of the end user
- how to empathise with a user
- how to define the problem

Students can:
- search for and gather information about a potential client
- determine and justify which information is relevant to the project
- create a persona for a potential client or target audience
- write a problem definition 

### Teaching strategies

Use scaffolded instruction so students achieve the lesson objectives.

**Preparation**

- in planning the teaching of this project, it was determined that students must
  be able to clearly articulate the problem that is to be solved, as it is central
  to guiding their decision making later in the project
- to do this it was decided that the design thinking process of empathising with the
  end user was an important technique to learn
- a template worksheet, with examples, was created to scaffold student learning 
- no prerequisite knowledge is needed, however this process was introduced in project 1,
  we build on it here

**Start the lesson**

10 minutes.

Student prompt: Stan Lee is the creator of many Marvel superheros.
As you [watch this video](https://www.youtube.com/watch?v=AhCyVP5bTkQ)
write some thoughts on what you think he would need if he were surfing the
Internet for new character ideas?

Use the following powerpoint slide to project some focus questions on the board:

```{eval-rst}
:download:`Stan Lee focus questions <files/stan-lee-research-q.pptx>`
```

As the video plays, demonstrate to the students what to do by writing
one point for each of the focus questions.

After the video, pick one point and explain why it is relevant with respect
to creating a website that would target Stan Lee.

Invite students to share responses to the other questions, and explain why
it is relevant to developing the website.

State the goal of the lesson:
- students will create a problem statement
- they will do this by gaining an insight into their character
- the lesson will proceed in guided stages:
  - research your show's creator
  - create a persona for them
  - identify an insight, a problem they have, that your website will solve
  - as a team, write a problem statement that will guide your project

**Learning: research the show's creator**

20 minutes

As demonstrated by the Stan Lee example,
each student in the team searches the Internet for a source of information about
the creator. The information can be in the form of video, audio, transcript, blog entry,
magazine or news article, etc.
Students should make a choice within 5 minutes - don't waste time doing too much research. 

As students read or view the information, they write down everything this person reveals about
themselves, using the demonstrated focus questions and the persona template provided.

```{eval-rst}
:download:`persona development template <files/persona-development.docx>`
```

Move around the classroom, monitoring progress on the task and ensuring they
are using a reliable source of information.

**Learning: create a persona**

20 minutes

With the research done and the students having noted some key points,
they now write one or two paragraphs, which gives an overview of their user.
Discuss the example in the persona template to encourage students to
identify some 'hard facts' and 'personal interests and values'. Invite
students to volunteer some hard facts or personal interests about their creator
to the rest of the class.

Every student must create a persona.

```{eval-rst}
:download:`persona development template <files/persona-development.docx>`
```

Move around the classroom, asking students what information they think
is important to include in their persona.

End of class 1.

**Learning: Empathise by creating scenarios**

15 minutes

Rengage students' learning with a quick review of research and persona creation.
Today they'll use that knowledge to write a scenario, develop an insight,
and define the central problem their website will solve.

What is empathy? And how it helps us design solutions.
 
[Watch this airport design video](https://www.interaction-design.org/literature/topics/empathy)
about empathising with users.

Classroom discussion:
- what are a passenger's goals?
- what do they have to do to achieve it?
- how did the airport in Italy impede passengers from achieving their goal?
- how did the airport in Dubai help passengers achieve their goal?
- which experience would you prefer and why?

Now let's look at a plausible, real-life scenario for Stan Lee in the persona-
development template. It:
- describes the location (his home office)
- a time of day (late at night)
- issues that are causing him problems (illness and lack of time)
- and a need (to prepare for an important meeting)

```{eval-rst}
:download:`persona development template <files/persona-development.docx>`
```

Each student now imagines their user (the show's creator) in a plausible, real-life scenario. Using the
persona development template, they write a
scenario (each student in the team must create a unique scenario).

Move around the classroom. Ask students to tell your their scenario. Guide
students if the scenario is implausible or not relevant to the website task at hand.

**Learn: empathise by finding an insight**

10 minutes

Using the table in part 4 of the persona development template, show students how they
can break their scenario down to clearly identify:
- the user
- what their need is in this context
- an insight into a problem they need solved

```{eval-rst}
:download:`persona development template <files/persona-development.docx>`
```

Towards the end, invite some students to read their insights.
Offer suggestions on how the insight could be improved to help the in
defining the problem, which is covered next.

**Learn: define the problem**

15 minutes

Explain to the class the _point of view_ technique for defining the problem.
Identifying the three components:
- user and information
- user's needs
- compelling reasons

Then show where the Stan Lee example given in part 5 of the persona development template
meets the _point of view_ criteria, and how it uses the insights developed at step 4.

Use the [point of view problem statement resource](http://tualatinpsychology.weebly.com/uploads/5/7/2/8/5728202/point-of-view-problem-statement.pdf)
to critically assess the problem statement and how it could be improved.

In teams, students now use the _Point of View_ technique to write their problem statement.

```{eval-rst}
:download:`persona development template <files/persona-development.docx>`
```

**End the lesson**

10 minutes

This is a class discussion. Moving around the class, each team recites their
problem statement. After the first team recites their problem statement, offer them suggestions
on how it could be improved.
Then after each subsequent team, ask other teams to comment on how it might be improved.
Refer to the criteria in 
[the point of view problem statement resource](http://tualatinpsychology.weebly.com/uploads/5/7/2/8/5728202/point-of-view-problem-statement.pdf).


### Resources

- [Stan Lee interview](https://www.youtube.com/watch?v=AhCyVP5bTkQ)
- [Airport design video](https://www.interaction-design.org/literature/topics/empathy)
- [Point of view problem statement resource](http://tualatinpsychology.weebly.com/uploads/5/7/2/8/5728202/point-of-view-problem-statement.pdf)

```{eval-rst}
:download:`persona development template <files/persona-development.docx>`
```
